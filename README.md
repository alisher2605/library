# Library

A microservice architecture based system for managing book library.
Currently this application supports interaction between the main entities:

- 1. Book
- 2. Member
- 3. Author

And two connecting entities, through which you can receive information about ownership of a book:

- 1. AuthorBook
- 2. MemberBook

Additionally added Apache Kafka server with zookeeper, producer and consumer functions included. In current project, author service is producer and sends JSON message to a new Consumer service

> The ER diagram of the system
> ![](ER1.png)
